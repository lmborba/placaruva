class Problem < ActiveRecord::Base

  has_many :runs

  default_scope order('number')

end
