require 'nokogiri'
require 'open-uri'

class User < ActiveRecord::Base

  has_many :runs

  def self.generate(params)
    user = self.new(params)
    uri = 'http://uhunt.felix-halim.net/api/uname2uid/' + user.uva_login
    doc = Nokogiri::HTML(open(uri))
    user.uva_id = doc.content
    user
  end

  def scoreboard
    accepts = Run.where(:user_id => self.id, :verdict => 1).group(:problem_id).select("*,MIN(runs.current_time) AS current_time")
    errors = []
    sumtime = 0
    accepts.each do |problem|
      sumtime = sumtime + problem.current_time
      x = Run.joins(:problem).where("problems.id = ? AND runs.user_id = ? AND runs.current_time < ? AND runs.verdict = ?",problem.problem_id,self.id,problem.current_time,0).group(:problem_id).select("*, COUNT(verdict) AS verdict")
      errors << x[0]
      unless x[0].nil?
        sumtime = sumtime + 20*(x[0].verdict)
      end
    end
    return [accepts.length,sumtime,accepts,errors,self]
  end

  def self.complete_scoreboard
    v = []
    User.all.each do |user|
      v << user.scoreboard
    end
    v.sort_by { |points| [-points[0], points[1]] }
  end

end
