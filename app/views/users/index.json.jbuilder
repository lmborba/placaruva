json.array!(@users) do |user|
  json.extract! user, :uva_id, :uva_name, :uva_login
  json.url user_url(user, format: :json)
end