json.array!(@runs) do |run|
  json.extract! run, :user_uva_id, :problem_number, :verdict, :official_run
  json.url run_url(run, format: :json)
end