require 'nokogiri'
require 'open-uri'

task :update => :environment do

  initial = DateTime.now

  while true

    uri = 'http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=19'
    doc = Nokogiri::HTML(open(uri))

    doc.css("#col3_content_wrapper table tr:not(.sectiontableheader)").each do |tr|
      arr = tr.css("td")
      uva_run_id = arr[0].content
      uva_problem_number =  arr[1].content
      link_user = /index.php\?option=com_onlinejudge&Itemid=19&page=show_authorstats&userid=(\d+)/.match(arr[3].css("a")[0]['href'])[1]
      verdict = arr[4].content
      date = DateTime.now
      date = (date.to_i - initial.to_i)/60
      if verdict == "Accepted"
        verdict = 1
      else
        verdict = 0
      end
      run = Run.where(:official_run => uva_run_id)
      problem = Problem.where(:number => uva_problem_number)
      user = User.where(:uva_id => link_user)
      if (not run.exists?)
        if (problem.exists? and user.exists?)
          puts "HERE"
          puts uva_run_id
          prob = problem.first
          usr = user.first
          run = Run.new
          run.user = usr
          run.problem = prob
          run.verdict = verdict
          run.official_run = uva_run_id
          run.current_time = date
          run.save
        else
          run = nil
        end
      else
        run = run.first
      end
      unless run.nil?
        if (run.verdict != verdict)
          run.verdict = verdict
          run.save
        end
      end
    end


    sleep 10

  end

end
