class ConvertProblemNumberToInt < ActiveRecord::Migration

  def up
    change_column :problems, :number, :integer
  end

  def down
    change_column :problems, :number, :string
  end

end
