class CreateRuns < ActiveRecord::Migration
  def change
    create_table :runs do |t|
      t.integer :user_id
      t.integer :problem_id
      t.integer :verdict
      t.string :official_run

      t.timestamps
    end
  end
end
