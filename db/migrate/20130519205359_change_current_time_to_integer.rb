class ChangeCurrentTimeToInteger < ActiveRecord::Migration
  def up
    change_column :runs, :current_time, :integer
  end

  def down
    change_column :runs, :current_time, :datetime
  end
end
