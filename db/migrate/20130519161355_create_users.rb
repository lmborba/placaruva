class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :uva_id
      t.string :uva_name
      t.string :uva_login

      t.timestamps
    end
  end
end
