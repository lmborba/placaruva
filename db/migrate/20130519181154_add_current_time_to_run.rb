class AddCurrentTimeToRun < ActiveRecord::Migration
  def change
    add_column :runs, :current_time, :datetime
  end
end
